Name:           gcstar
Version:        1.8.0
Release:        1%{?dist}
Summary:        Personal collections manager

Group:          Applications/Internet
License:        GPLv2+
URL:            https://gitlab.com/GCstar/GCstar
Source:         GCstar-v%{version}.tar.gz
BuildArch:      noarch

AutoReqProv:    no
Requires:       perl-interpreter, perl-Gtk3, perl-HTML-Parser, perl-libwww-perl
Requires:       perl-LWP-Protocol-https, perl-Date-Calc, perl-GDGraph
Requires:       perl-DateTime-Format-Strptime, perl-XML-Parser
Requires:       perl-XML-Simple, perl-GDGraph, perl-Archive-Tar
Requires:       perl-Archive-Zip, perl-GDTextUtil
Requires:       perl-MP3-Info, perl-Time-Piece, perl-Image-ExifTool, perl-JSON
Requires:       perl-Gtk3-SimpleList, webp-pixbuf-loader
Requires:       perl-Locale-Codes, perl-FindBin, perl-Unicode-Normalize
Requires:       perl-threads, perl-threads-shared
Recommends:     perl-MP3-Tag, perl-Net-FreeDB, perl-Ogg-Vorbis-Header-PurePerl

%define __os_install_post %{nil}            # don't strip files
%define _unpackaged_files_terminate_build 0 # allow unpackaged files 

%description
GCstar is an application for managing personal collections (books, films, games...).
Detailed information on each item can be automatically retrieved from the internet.
Lending collection items to other people can be tracked (date, person, status).
New collection types can be created by users.

%description -l fr
GCstar - Application permettant de gérer des collections personnelles (films, livres, jeux, timbres...).
Des recherches internet permettent de compléter les descriptions des éléments. Le prêt d'éléments est géré.
De nouveaux types de collections peuvent être ajoutés par les utilisateurs.

%prep

%setup -q -n gcstar

%build

%install

./install --prefix=%{buildroot}%{_prefix} --verbose --text --noclean --fhs

%files

%doc

%{_bindir}/%{name}
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/*/mimetypes/application-x-%{name}.png
%{_datadir}/mime/application/x-%{name}.xml
%{_datadir}/mime/packages/%{name}.xml
%{_datadir}/pixmaps/%{name}.png
%{_mandir}/man1/%{name}.1.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor
update-desktop-database
update-mime-database %{_datadir}/mime

%postun
gtk-update-icon-cache %{_datadir}/icons/hicolor
update-desktop-database
update-mime-database %{_datadir}/mime

%changelog
* Sun Mar 10 2024 Kerenoc
- use FHS option to install without patching

* Mon Apr 3 2023 Kerenoc
- prepare 1.8.0

* Tue Mar 28 2023 Kerenoc
- remove Provides fields
- remove duplicatesv

* Mon Dec 9 2019 Kerenoc
- Adaptation for version 1.7.3 Gtk3

* Thu Nov 8 2018 Nuno Dias <Nuno.Dias@gmail.com> - 1.7.2-1.ndias
- Release Version 1.7.2 from gitlab

* Wed Jul 18 2018 Nuno Dias <Nuno.Dias@gmail.com> - 1.7.2-gitlab20180718.1.ndias
- Version 1.7.2 from git
