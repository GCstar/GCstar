package GCPlugins::GCbooks::GCbooksAdlibrisCommon;

###################################################
#
#  Copyright 2005-2010 Christian Jodar
#  Copyright 2017-2024 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCbooks::GCbooksCommon;

{
    package GCPlugins::GCbooks::GCbooksAdlibrisPluginsBase;

    use base qw(GCPlugins::GCbooks::GCbooksPluginsBase);

    use URI::Escape;
    use Encode;

    use JSON qw( decode_json );

    sub parse
    {
        my ($self, $html) = @_;

        if ($self->{parsingList} || ! $self->{json})
        {
            HTML::Parser::parse($self,$html);
        }
        else
        {
            my $item = $self->{json}->{ProductVariants}->[0];

            $item->{Description} =~ s/(<p>|<\/?b>|<\/?i>)//g;
            $item->{Description} =~ s/(<br>|<\/p>)/\n/g;
            $self->{curInfo}->{title}        = $item->{Title};
            $self->{curInfo}->{isbn}         = $item->{Ean};
            $self->{curInfo}->{format}       = $item->{Format};
            $self->{curInfo}->{language}     = $item->{Language};
            $self->{curInfo}->{description}  = $item->{Description};
            $self->{curInfo}->{cover}        = $item->{Images}->{ProductImages}->[0]->{Url};
            $self->{curInfo}->{pages}        = $item->{ProductInfo}->{Page}->{Values}->[0]->{Value};
            $self->{curInfo}->{artist}       = $item->{ProductInfo}->{Illustrators}->{Values}->[0]->{Value};
            $self->{curInfo}->{publisher}    = $item->{ProductInfo}->{Publisher}->{Values}->[0]->{Value};
            $self->{curInfo}->{serie}        = $item->{ProductInfo}->{Series}->{Values}->[0]->{Value};
            $self->{curInfo}->{translator}   = $item->{ProductInfo}->{Translators}->{Values}->[0]->{Value};
            $self->{curInfo}->{publication}  = $item->{ProductInfo}->{Published}->{Values}->[0]->{Value};
            foreach my $author (@{$item->{Authors}})
            {
                $self->{curInfo}->{authors} .= $author.", ";
            }
            $self->{curInfo}->{authors} =~ s/, $//;
            my @genresPaths = split(/\|/, $item->{CategoryNamePath});
            foreach my $genresPath (@genresPaths)
            {
                my @genres = split(/\//, $genresPath);
                foreach my $genre (@genres)
                {
                    $self->{curInfo}->{genre} .= $genre.", "
                      if ($self->{curInfo}->{genre} !~ m/$genre/ && $genre ne 'Böcker');
                }
            }
            $self->{curInfo}->{genre} =~ s/, $//;
        }
    }

    sub start
    {
        my ($self, $tagname, $attr, $attrseq, $origtext) = @_;

        if ($self->{parsingList})
        {
            if ($tagname eq 'a' && $attr->{class} && $attr->{class} eq 'search-result__product__name')
            {
                $self->{itemIdx}++;
                $self->{itemsList}[$self->{itemIdx}]->{url} = "https://www.adlibris.com" . $attr->{href};
                $self->{isTitle} = 1 ;
            }
            elsif ($tagname eq 'a' && $attr->{itemprop} && ($attr->{itemprop} =~ m/author/))
            {
                $self->{isAuthor} = 1 ;
            }
            elsif ($tagname eq 'span' && $attr->{class} && ($attr->{class} =~ m/book-format/))
            {
                $self->{isFormat} = 1 ;
            }
        }
        else
        {
            if ($tagname eq 'title')
            {
                $self->{isTitle} = 1;
            }
            elsif ($tagname eq 'span' && $attr->{class} && $attr->{class} =~ /product__attribute__name/)
            {
                $self->{isAttribute} = 1;
            }
            elsif ($tagname eq 'div' && $attr->{id} && $attr->{id} =~ /acc-description/)
            {
                $self->{isDescription} = 1;
            }
            elsif ($tagname eq 'img' && ! $self->{curInfo}->{cover})
            {
                $self->{curInfo}->{cover} = $attr->{src};
            }
        }
    }

    sub end
    {
        my ($self, $tagname) = @_;

    }

    sub text
    {
        my ($self, $origtext) = @_;

        # Enleve les blancs en debut de chaine
        $origtext =~ s/^\s+//;
        $origtext =~ s/\s+$//;
        $origtext =~ s/^[\r\n]+$//;
        return if (! $origtext);

        if ($self->{parsingList})
        {
            if ($self->{isTitle})
            {
                $self->{itemsList}[$self->{itemIdx}]->{title} = $origtext;
                $self->{isTitle} = 0 ;
            }
            elsif ($self->{isAuthor})
            {
                if ($self->{itemsList}[$self->{itemIdx}]->{authors} eq '')
                {
                    $self->{itemsList}[$self->{itemIdx}]->{authors} = $origtext;
                }
                else
                {
                    $self->{itemsList}[$self->{itemIdx}]->{authors} .= ", " . $origtext;
                }
                $self->{isAuthor} = 0 ;
            }
            elsif ($self->{isFormat})
            {
                $self->{itemsList}[$self->{itemIdx}]->{format} = $origtext;
                $self->{isFormat} = 0 ;
            }
        }
        else
        {
            if ( $self->{isTitle} eq 1)
            {
                $self->{curInfo}->{title} = $origtext;
                $self->{isTitle} = 0;
            }
            elsif ( $self->{isDescription} eq 1)
            {
                $self->{curInfo}->{description} = $origtext;
                $self->{isDescription} = 0;
            }
            elsif ($self->{searching} && $origtext ne ':')
            {
                $self->{curInfo}->{$self->{searching}} = $origtext;
                $self->{isAttribute} = 0;
                $self->{curInfo}->{publication} =~ s/([0-9]*)\.([0-9]*)\.([0-9]*)/\3\/\2\/\1/
                    if $self->{searching} eq 'publication';
                undef $self->{searching};
            }
            else
            {
                while (my ($key, $value) = each(%{$self->{attributeMap}}))
                {
                    if ($origtext =~ /^$key *:*$/)
                    {
                        $self->{searching} = $value;
                        $self->{isAttribute}= 2;
                    }
                }

            }
        }
    }

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();
        bless ($self, $class);

        $self->{hasField} = {
            title => 1,
            authors => 1,
            publication => 0,
            format => 1,
            edition => 0,
        };

        $self->{isLang} = 'se';

        $self->{attributeMap} = {
                    'Forfatter'     => 'authors',
                    'Oversetter'    => 'translator',
                    'Illustratør'   => 'artist',
                    'ISBN'          => 'isbn',
                    'Språk'         => 'language',
                    'Serie'         => 'serie',
                    'Utgitt'        => 'publication',
                    'Forlag'        => 'publisher',
                    'Antall sider'  => 'pages',
                    'Verkt'         => 'format',
        };

        return $self;
    }

    sub preProcess
    {
        my ($self, $html) = @_;

        $self->{isTitle} = 0;
        $self->{isAuthor} = 0;
        $self->{isFormat} = 0;
        $self->{isPublisher} = 0;
        $self->{isISBN} = 0;
        $self->{isPublicationAndPages} = 0;
        $self->{isLangAndReliure} = 0;
        $self->{isDescription} = 0;

        if (! $self->{parsingList})
        {
            my $lang = $self->getLang();
            $self->{json} = "";
            if ($html =~ /window.pageData/)
            {
                $html =~ s|.*window.pageData = ||s;
                $html =~ s|<\/script>.*||s;
                $html =~ s|;[\n\r ]*$||;
                my $encoding = $self->getCharset();
                if ($encoding ne 'UTF-8')
                {
                    eval { $html = decode($encoding,$html); };
                }
                $html = encode('utf-8',$html);
                $self->{json} = decode_json($html);
            }
        }

        return $html;
    }

    sub getSearchUrl
    {
        my ($self, $word) = @_;

        return "https://www.adlibris.com/". $self->{isLang}."/".$self->{searchKey}."?q=" . $word.$self->{searchFilter};
    }

    sub getItemUrl
    {
        my ($self, $url) = @_;

        return $url;
    }

    sub getAuthor
    {
        return 'TPF - Kerenoc';
    }

    sub getSearchFieldsArray
    {
        return ['isbn', 'title'];
    }
}

1;
