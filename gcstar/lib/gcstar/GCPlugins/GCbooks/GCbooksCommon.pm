package GCPlugins::GCbooks::GCbooksCommon;

###################################################
#
#  Copyright 2005-2010 Christian Jodar
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;

use GCPlugins::GCPluginsBase;

{
    package GCPlugins::GCbooks::GCbooksPluginsBase;

    use base qw(GCPluginParser);
    use HTML::Entities;

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();
        bless ($self, $class);

        $self->initFieldNames();

        return $self;
    }

    sub getSearchFieldsArray
    {
        return ['title', 'authors', 'isbn'];
    }

    sub getEanField
    {
        my $self = shift;
        my $fields = $self->getSearchFieldsArray;
        return 'isbn'
            if grep($fields, 'isbn');
        return undef;
    }

    sub initFieldNames
    {
        my $self = shift;

        $self->{searchType} = 'books';
        $self->{searchFieldsArray} = ['title', 'authors', 'isbn'];

        # some plugins are used for books, gadgets and comics that use different names for some fields
        $self->{titleField}       = 'title';
        $self->{seriesField}      = 'serie';
        $self->{coverField}       = 'cover';
        $self->{illustratorField} = 'artist';
        $self->{colouristField}   = 'artist';
        $self->{lettererField}    = 'artist';
        $self->{publisherField}   = 'publisher';
        $self->{publicationField} = 'publication';
        $self->{authorField}      = 'authors';
        $self->{descriptionField} = 'description';
        $self->{nbpagesField}     = 'pages';
        $self->{collectionField}  = 'edition';
    }
}

1;
